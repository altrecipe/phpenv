# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:
    __PREFIX__nginx:
        # Пропишем какой образ мы хотим использовать
        image: nginx:latest
        # Назовем свой контейнер по красивому
        container_name: __PREFIX__nginx
        # Проброс портов
        ports:
            - "80:80"
            - "443:443"
        # Проброс папок
        volumes:
            - ./docker/nginx/core:/etc/nginx/conf.d
            - ./project/:/var/www/site/
            - ./docker/nginx/Logs:/var/log/nginx/
            - ./docker/nginx/html:/usr/share/nginx/html/
        # Укажем зависимости
        links:
            - __PREFIX__php

    __PREFIX__db:

        image: mariadb

        ports:
            - "3306:3306"

        container_name: __PREFIX__db
        # Пропишем настройки, предлагаю использовать вместо 
        # mypassword более сложный пароль, он принадлежит root
        environment:

            - MYSQL_ROOT_PASSWORD=__MYSQL_ROOT_PASS__
            - MYSQL_DATABASE=__MYSQL_DBNAME__
            - MYSQL_USER=__MYSQL_DBUSER__
            - MYSQL_PASSWORD=__MYSQL_DBPASS__


        volumes:
            - ./docker/mysql:/var/lib/mysql


    __PREFIX__php:
        # Билдим с помощью dockerfile указав директорию где он лежит
        build: ./docker/php

        container_name: __PREFIX__php

        volumes:
            - ./project:/var/www/site

        links:
            - __PREFIX__db:db


    __PREFIX__phpmyadmin:

        image: phpmyadmin/phpmyadmin

        container_name: __PREFIX__phpmyadmin

        ports:

            - 8090:80

        links:
            - __PREFIX__db:db
