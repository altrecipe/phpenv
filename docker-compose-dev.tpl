# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:
    __PREFIX__nginx:
        # Проброс папок
        volumes:
            - __PREFIX__project-sync:/var/www/site:nocopy


    __PREFIX__php:
        volumes:
            - __PREFIX__project-sync:/var/www/site:nocopy
        expose:
            - "13136"


volumes:
    __PREFIX__project-sync:
        external: true
